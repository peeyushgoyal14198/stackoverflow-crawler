const PRIMARY_URL = 'https://stackoverflow.com/questions'
const PAGINATION_URL = 'https://stackoverflow.com/questions?tab=newest&page='


module.exports = {
    PRIMARY_URL,
    PAGINATION_URL
}