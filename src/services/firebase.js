const firebaseAdmin = require('firebase-admin');


/**
 *  Saves information to firestore
 * @param {*} infoToSave (pass data as map)
 * @returns 
 */
async function saveInfo(infoToSave) {
    return await firebaseAdmin.firestore().collection('stackoverflow').add({
        ...infoToSave
    })
}


/**
 * Saves check point to firestore 
 * @param {*} checkPoint (usually a page no)
 * @returns 
 */
async function saveCheckPoint(checkPoint) {
    return await firebaseAdmin.firestore().collection('check_points').doc('stackoverflow').set({
        checkPoint: checkPoint
    })
}

/**
 * Retrives check point
 * @returns 
 */
async function retrieveCheckPoint() {
    const doc = await firebaseAdmin.firestore().collection('check_points').doc('stackoverflow').get()
    if (doc.exists) {
        return doc.data().checkPoint
    } else {
        return 1
    }
}

module.exports = {
    saveCheckPoint,
    retrieveCheckPoint,
    saveInfo
}