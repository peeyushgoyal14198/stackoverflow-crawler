const express = require("express");
const cors = require("cors");
require("dotenv").config();
const firebaseAdmin = require("firebase-admin");
var serviceAccount = require(process.env.FIREBASE_SERVICE_ACCOUNT);
const {
    initCrawler
} = require("./src/services/crawler");
firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: process.env.FIREBASE_DATABSE_URL
});
const app = express();
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cors());

const PORT = 5000 || process.env.PORT;


initCrawler()


app.listen(PORT, console.log(`Server started at PORT: ${PORT}`));