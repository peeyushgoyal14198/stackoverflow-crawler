# Stack over flow crawler

# Prerequisites

## Install Nodejs or have a Docker setup

```
https://nodejs.org/en/download/

---- or ----

https://www.docker.com/products/docker-desktop
```

## Clone the project

```
$ git clone https://gitlab.com/peeyushgoyal14198/stackoverflow-crawler.git
$ cd crawler
$ npm install
```

# Configuration

Create `.env` file with the following content

> Note that some values are secret and MUST not be checked into the VCS or shared publicly.

```

PORT = 4000 (if you need to setup you own port please change the port here and also docker-compose.yml)
FIREBASE_DATABSE_URL = https://peeyush-4af46.firebaseio.com
FIREBASE_SERVICE_ACCOUNT = ./config/peeyush-4af46-firebase-adminsdk-ervdr-75bda8e660.json
```

# Build and run

```
$ npm run start or docker-compose up -d

need docker logs then run

$ docker ps (copy container id )
$ docker logs {container id}

```

# Database limit issue

In free tier firebase allows on 50,000 document write requests for a day. You can solve this by adding your own firebase config or hard code total no of pages in crawl function here (/services/crawler/initcCrawler/ (crawl(3, 1)))

In order to get the service key from Firebase, follow these steps:

- Create you firebase project and add web app
- Go to firebase project settings
- Navigate to service accounts tab
- Select firebase admin sdk
- Generate new private key from bottom
- Download and save it to config folder
- Chnage the name of file in .env for variable FIREBASE_SERVICE_ACCOUNT
