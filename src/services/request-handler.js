const axios = require('axios')

const requestHandler = axios.default

module.exports = {
    requestHandler
}