const cheerio = require("cheerio");

const {
    PRIMARY_URL,
    PAGINATION_URL
} = require("../constants/url-contants");
const {
    saveInfo,
    saveCheckPoint,
    retrieveCheckPoint
} = require("./firebase");
const {
    requestHandler
} = require("./request-handler")

/**
 *  Calling this function will start crawling prcoess
 */
async function initCrawler() {
    try {
        const totaltPages = await getTotalPagesToScrap()
        console.log(`I will scrap these many pages ${totaltPages}`)
        const checkPoint = await retrieveCheckPoint()
        await crawl(totaltPages, checkPoint)
    } catch (error) {
        console.error(error)
    }
}


/**
 * Crawls data from the url 
 * @param {*} totaltPages 
 * @param {*} pageNo 
 * @returns 
 */
async function crawl(totaltPages, pageNo) {

    if (pageNo <= totaltPages) {
        let fetchArray = buildFetchArray(pageNo, totaltPages)

        console.log(`No of concurrent requests ${fetchArray.length}`)
        const responses = await requestHandler.all(fetchArray)
        responses.forEach((value) => {
            extractAndSaveInfo(value.data)
        })
        pageNo = pageNo + 5
        saveCheckPoint(pageNo)
        return crawl(totaltPages, pageNo)
    }
    return
}

/**
 * Extracts data from html and saves to database
 * @param {*} htmlData 
 */
function extractAndSaveInfo(htmlData) {

    const $ = cheerio.load(htmlData);

    $("#questions").children().each((_, ele) => {

        let answers = $(ele).find(".status.unanswered").children().text()
        let votes = $(ele).find(".vote-count-post").children().text()
        let views = $(ele).find(".views").attr('title');
        let question = $(ele).find("a").text()

        saveInfo({
            'votes': parseInt(votes),
            'views': views,
            'question': question,
            'answers': parseInt(answers)
        })

    });
}


/**
 * Builds and returns request handler array
 * @param {*} pageNo 
 * @returns 
 */
function buildFetchArray(pageNo, totaltPages) {

    const length = pageNo + 5 >= totaltPages ? totaltPages + 1 : pageNo + 5
    let fetchArray = []

    for (let i = pageNo; i < length; i++) {
        let goto_url = PAGINATION_URL + `${i}`
        fetchArray.push(requestHandler.get(goto_url))
    }

    return fetchArray;
}



/**
 * Total pages to scrap
 * @returns 
 */
async function getTotalPagesToScrap() {

    const goto_url = PRIMARY_URL

    const value = await requestHandler.get(goto_url)
    const $ = cheerio.load(value.data);
    let totalPages = $('#mainbar > div.s-pagination.site1.themed.pager.fl > a:nth-child(7)').text()

    return parseInt(totalPages)
}

module.exports = {
    initCrawler
}